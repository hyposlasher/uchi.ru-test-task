var gulp = require('gulp')
var server = require('gulp-server-livereload');
var jade = require('gulp-jade');

gulp.task('jade', function() {
  gulp.src('./app/*.jade')
    .pipe(jade())
    .pipe(gulp.dest('./app/'))
});

gulp.task('watch', function(){
 gulp.watch('./app/*.jade',['jade']);
});

gulp.task('webserver', function() {
  gulp.src('./app')
    .pipe(server({
      livereload: true,
      directoryListing: {
        enable: true,
        path: 'app'
      },
      open: true
    }));
});

gulp.task('default',[
  'webserver',
  'jade',
  'watch'
]);
